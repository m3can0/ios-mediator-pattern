//
//  SplashController.swift
//  app
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import UIKit
import lib

class SplashController: UIViewController {
    
    private let controllerNameLabel: UILabel = {
        let label = UILabel()
        label.text = "SplashController"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        view.addSubview(controllerNameLabel)
        
        controllerNameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            controllerNameLabel.heightAnchor.constraint(equalToConstant: 44),
            controllerNameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            controllerNameLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            controllerNameLabel.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor),
            controllerNameLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
            ])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            let controller = LibViewController()
            controller.mediator = Mediator()
            self.present(controller, animated: true, completion: nil)
        }
    }
}
