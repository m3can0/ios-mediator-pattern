//
//  Mediator.swift
//  app
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import Foundation
import lib

class Mediator : MediatorProtocol {
    func startControllerExample(currentController: UIViewController) {
        let controller = AppController()
        currentController.present(controller, animated: true, completion: nil)
    }
    func getIsChecked() -> Bool {
        return LocalDataManager().getIsChecked()
    }
    
    func setIsChecked(isChecked: Bool) {
        LocalDataManager().setIsChecked(isChecked: isChecked)
    }
    
    func fetchCommentList(onCommentCallback: @escaping ([Comment]) -> Void) {
        //Mock api call here
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            onCommentCallback([Comment(id: 1, title: "title 1"), Comment(id: 2, title: "title 2")])
        }
    }
}
