//
//  ViewController.swift
//  app
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import UIKit
import lib

class AppController: UIViewController {
    
    private let controllerNameLabel: UILabel = {
        let label = UILabel()
        label.text = "AppController"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    private let startLibControllerButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Back to LibController", for: .normal)
        btn.backgroundColor = UIColor.lightGray
        btn.addTarget(self, action: #selector(onStartLibController), for: .touchUpInside)
        return btn
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        view.addSubview(controllerNameLabel)
        
        controllerNameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            controllerNameLabel.heightAnchor.constraint(equalToConstant: 44),
            controllerNameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            controllerNameLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            controllerNameLabel.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor),
            controllerNameLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
            ])
        
        view.addSubview(startLibControllerButton)
        
        startLibControllerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            startLibControllerButton.heightAnchor.constraint(equalToConstant: 44),
            startLibControllerButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            startLibControllerButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor),
            startLibControllerButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
            ])
    }
    
    @objc func onStartLibController(sender: UIButton!) {
        let controller = LibViewController()
        controller.mediator = Mediator()
        present(controller, animated: true, completion: nil)
    }
}

