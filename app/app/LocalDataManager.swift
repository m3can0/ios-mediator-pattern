//
//  LocalDataManager.swift
//  app
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import Foundation

class LocalDataManager {
    private let preferences = UserDefaults.standard
    
    private let isCheckedKey = "isChecked"
    
    func getIsChecked() -> Bool{
        return preferences.bool(forKey: isCheckedKey)
    }
    
    func setIsChecked(isChecked: Bool) {
        preferences.set(isChecked, forKey: isCheckedKey)
        preferences.synchronize()
    }
}
