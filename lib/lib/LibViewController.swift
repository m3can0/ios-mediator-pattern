//
//  LibViewController.swift
//  lib
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import Foundation

import UIKit

public class LibViewController: UIViewController {
    
    public var mediator: MediatorProtocol? = nil
    
    private let startControllerLabel: UILabel = {
        let label = UILabel()
        label.text = "Present controller example"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    private let startControllerButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Start AppController", for: .normal)
        btn.backgroundColor = UIColor.lightGray
        btn.addTarget(self, action: #selector(onStartController), for: .touchUpInside)
        return btn
    }()
    
    private let isCheckedLabel: UILabel = {
        let label = UILabel()
        label.text = "Persistance injection example"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    private let isCheckedSwitch: UISwitch = {
        let uiSwitch = UISwitch()
        uiSwitch.addTarget(self, action: #selector(onIsCheckedChanged), for: .valueChanged)
        return uiSwitch
    }()
    
    private let networkInjectionLabel: UILabel = {
        let label = UILabel()
        label.text = "Network injection example"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    private let networkResponseLabel: UILabel = {
        let label = UILabel()
        label.text = "Loading..."
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        return label
    }()
    
    override public func loadView() {
        super.loadView()
        view.backgroundColor = .white
        setupStartControllerLabel()
        setupStartControllerButton()
        setupIsCheckedLabel()
        setupIsCheckedSwitch()
        setupNetworkInjectionLabel()
        setupNetworkResponseLabel()
    }

    
    func setupStartControllerLabel() {
        view.addSubview(startControllerLabel)
        
        startControllerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            startControllerLabel.heightAnchor.constraint(equalToConstant: 44),
            startControllerLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            startControllerLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16),
            startControllerLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
            ])
    }
    
    func setupStartControllerButton() {
        view.addSubview(startControllerButton)
        
        startControllerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            startControllerButton.heightAnchor.constraint(equalToConstant: 44),
            startControllerButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            startControllerButton.topAnchor.constraint(equalTo: startControllerLabel.bottomAnchor, constant: 16),
            startControllerButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
            ])
    }
    
    func setupIsCheckedLabel() {
        view.addSubview(isCheckedLabel)
        
        isCheckedLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            isCheckedLabel.heightAnchor.constraint(equalToConstant: 44),
            isCheckedLabel.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 16),
            isCheckedLabel.topAnchor.constraint(equalTo: startControllerButton.bottomAnchor, constant: 16),
            isCheckedLabel.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor, constant: -16)
            ])
    }
    
    func setupIsCheckedSwitch() {
        view.addSubview(isCheckedSwitch)
        
        isCheckedSwitch.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            isCheckedSwitch.heightAnchor.constraint(equalToConstant: 44),
            isCheckedSwitch.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 16),
            isCheckedSwitch.topAnchor.constraint(equalTo: isCheckedLabel.bottomAnchor, constant: 16),
            isCheckedSwitch.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor, constant: -16)
            ])
        
        let isOn = mediator?.getIsChecked() ?? false
        isCheckedSwitch.isOn = isOn
        isCheckedSwitch.setOn(isOn, animated: false)
    }
    
    func setupNetworkInjectionLabel() {
        view.addSubview(networkInjectionLabel)
        
        networkInjectionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            networkInjectionLabel.heightAnchor.constraint(equalToConstant: 44),
            networkInjectionLabel.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 16),
            networkInjectionLabel.topAnchor.constraint(equalTo: isCheckedSwitch.bottomAnchor, constant: 16),
            networkInjectionLabel.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor, constant: -16)
            ])
    }
    
    func setupNetworkResponseLabel() {
        view.addSubview(networkResponseLabel)
        
        networkResponseLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            networkResponseLabel.heightAnchor.constraint(equalToConstant: 44),
            networkResponseLabel.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor, constant: 16),
            networkResponseLabel.topAnchor.constraint(equalTo: networkInjectionLabel.bottomAnchor, constant: 16),
            networkResponseLabel.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor, constant: -16)
            ])
        
        mediator?.fetchCommentList() { (commentList) in
            var text = ""
            commentList.forEach() { (comment) in
                text += comment.id.description + ", "
            }
            self.networkResponseLabel.text = text
        }
    }
    
    @objc func onStartController(sender: UIButton!) {
        mediator?.startControllerExample(currentController: self)
    }
    
    @objc func onIsCheckedChanged(sender: UISwitch!) {
        mediator?.setIsChecked(isChecked: sender.isOn)
    }
}
