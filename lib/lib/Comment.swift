//
//  comment.swift
//  lib
//
//  Created by Nataniel P-Montpetit on 2021-10-08.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import Foundation

public struct Comment : CustomStringConvertible {
    let id: Int
    let title: String
    
    public init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
    
    public var description: String {
        return "{ id: " + id.description + ", title: " + title + " }"
    }
}
