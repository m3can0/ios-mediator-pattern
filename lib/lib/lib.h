//
//  lib.h
//  lib
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for lib.
FOUNDATION_EXPORT double libVersionNumber;

//! Project version string for lib.
FOUNDATION_EXPORT const unsigned char libVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <lib/PublicHeader.h>


