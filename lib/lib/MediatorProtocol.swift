//
//  MediatorProtocol.swift
//  lib
//
//  Created by Nataniel P-Montpetit on 2021-10-07.
//  Copyright © 2021 Nataniel P-Montpetit. All rights reserved.
//

import Foundation

public protocol MediatorProtocol {
    func startControllerExample(currentController: UIViewController)
    func getIsChecked() -> Bool
    func setIsChecked(isChecked: Bool)
    func fetchCommentList(onCommentCallback: @escaping ([Comment]) -> Void)
}
